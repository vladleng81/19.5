﻿#include <iostream>
#include <string>

class Animal
{
public:

	virtual void Voice() const = 0;

};

class Dog : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Woof!" << "\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Meow?" << "\n";
	}
};

class Cow : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Moo..." << "\n";
	}

};

class Pig : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Oink!" << "\n";
	}

};

class Chicken : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Clucks?" << "\n";
	}

};

int main()
{
	Animal* farm[5];
	farm[0] = new Dog();
	farm[1] = new Cat();
	farm[2] = new Cow();
	farm[3] = new Pig();
	farm[4] = new Chicken();

	for (Animal* a : farm)
		a->Voice();
}